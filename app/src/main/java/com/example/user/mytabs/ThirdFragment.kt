package com.example.user.mytabs


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_third.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ThirdFragment : Fragment() {

    override fun onCreateView(


        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    )
            : View? {
        // Inflate the layout for this fragment
        var mView = inflater.inflate(R.layout.fragment_third, container, false)

        places.add(Place("Zoka Cafe", "Discription", "http://juico.com/Images/About/Zoka.jpg"))
        places.add(
            Place(
                "Ward Resturant",
                "Discription",
                "https://media-cdn.tripadvisor.com/media/photo-s/05/b8/80/25/ward.jpg"
            )
        )
        places.add(
            Place(
                "InDoor Cafe",
                "Discription",
                "http://imgsrv2.pxdrive.com/pics/norm/281449.jpg"
            )
        )
        places.add(
            Place(
                "java u",
                "Discription",
                "https://pbs.twimg.com/media/B-xbaBjUMAAVGAn.jpg"
            )
        )


        places.add(
            Place(
                "xxxxxxxx",
                "Discription",
                "https://www.fakenamegenerator.com/images/sil-female.png"
            )
        )


        mView.mListView.adapter = PlacesAdapter(activity!!.applicationContext, places)

        return mView
    }

    var places = ArrayList<Place>()

    init {

    }
}
